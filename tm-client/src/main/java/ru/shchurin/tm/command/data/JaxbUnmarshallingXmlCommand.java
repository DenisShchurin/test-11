package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class JaxbUnmarshallingXmlCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "j-u";

    @NotNull
    private static final String DESCRIPTION = "Unmarshalling application data.";

    @NotNull
    private static final String UNMARSHALLING = "[UNMARSHALLING APPLICATION DATA ]";

    @NotNull
    private static final String FILE = "C:\\Users\\user\\IdeaProjects\\Data\\jaxb.xml";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(UNMARSHALLING);
        @NotNull final File file = new File(FILE);
        @NotNull final JAXBContext context = JAXBContext.newInstance(SavedApplication.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final SavedApplication savedApplication = (SavedApplication) unmarshaller.unmarshal(file);

        if (savedApplication.getProjects() != null)
        for (@NotNull final Project project : savedApplication.getProjects()) {
            serviceLocator.getProjectService().persist(project);
        }

        if (savedApplication.getTasks() != null)
        for (@NotNull final Task task : savedApplication.getTasks()) {
            serviceLocator.getTaskService().persist(task);
        }

        if (savedApplication.getUsers() != null)
        for (@NotNull final User user : savedApplication.getUsers()) {
            serviceLocator.getUserService().persist(user);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
