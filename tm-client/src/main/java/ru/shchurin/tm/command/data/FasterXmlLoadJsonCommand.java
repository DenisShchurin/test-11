package ru.shchurin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class FasterXmlLoadJsonCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "f-l-j";

    @NotNull
    private static final String DESCRIPTION = "Load application data.";

    @NotNull
    private static final String LOADING = "[LOAD APPLICATION DATA]";

    @NotNull
    private static final String FILE = "C:\\Users\\user\\IdeaProjects\\Data\\fasterxml.json";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(LOADING);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final File file = new File(FILE);
        SavedApplication savedApplication = mapper.readValue(file, SavedApplication.class);
        if (savedApplication.getProjects() != null)
            for (@NotNull final Project project : savedApplication.getProjects()) {
                serviceLocator.getProjectService().persist(project);
            }
        if (savedApplication.getTasks() != null)
            for (@NotNull final Task task : savedApplication.getTasks()) {
                serviceLocator.getTaskService().persist(task);
            }
        if (savedApplication.getUsers() != null)
            for (@NotNull final User user : savedApplication.getUsers()) {
                serviceLocator.getUserService().persist(user);
            }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
