package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class JaxbMarshallingXmlCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "j-m";

    @NotNull
    private static final String DESCRIPTION = "Marshalling application data.";

    @NotNull
    private static final String MARSHALLING = "[MARSHALLING APPLICATION DATA]";

    @NotNull
    private static final String FILE = "C:\\Users\\user\\IdeaProjects\\Data\\jaxb.xml";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(MARSHALLING);
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        @NotNull final SavedApplication savedApplication = new SavedApplication(projects, tasks, users);

        @NotNull final File file = new File(FILE);
        @NotNull final JAXBContext context = JAXBContext.newInstance(SavedApplication.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(savedApplication, file);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
