package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.*;

import org.eclipse.persistence.jaxb.MarshallerProperties;

public final class JaxbMarshallingJsonCommand extends AbstractCommand {
    private static final String COMMAND = "j-m-j";
    private static final String DESCRIPTION = "Marshalling application data.";
    private static final String MARSHALLING = "[MARSHALLING APPLICATION DATA ]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(MARSHALLING);
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final Class[] classes = new Class[] {SavedApplication.class};
        @NotNull final File file = new File("C:\\Users\\user\\IdeaProjects\\Data\\jaxb.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(classes, properties);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        @NotNull final SavedApplication savedApplication = new SavedApplication(projects, tasks, users);
        marshaller.marshal(savedApplication, file);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
