package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.*;

public class BootstrapClient implements ServiceLocator {
    private static final String GREETING = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private User currentUser;

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.shchurin.tm").getSubTypesOf(AbstractCommand.class);


    private void register(@Nullable final Class clazz) throws Exception {
        if (clazz == null) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();

        @Nullable final String cliCommand = command.getCommand();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void init() throws Exception {
        if (classes == null) return;
        for (@Nullable final Class clazz : classes)
            register(clazz);
    }


    public void start() throws Exception {
        System.out.println(GREETING);
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        final boolean commandIsNotSafe = !abstractCommand.isSafe();
        if (commandIsNotSafe && currentUser == null)
            throw new UserNotAuthorized();

        final boolean userAuthorizedAndHasNotAccessRight = currentUser != null &&
                !abstractCommand.getRoles().contains(currentUser.getRole());
        if (commandIsNotSafe && userAuthorizedAndHasNotAccessRight) throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable User currentUser) {
        this.currentUser = currentUser;
    }
}
