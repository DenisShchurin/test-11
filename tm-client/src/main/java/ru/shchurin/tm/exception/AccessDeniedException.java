package ru.shchurin.tm.exception;

public final class AccessDeniedException extends Exception {
    public AccessDeniedException() {
        super("Access Denied");
    }
}
