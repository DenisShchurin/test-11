package ru.shchurin.tm.exception;

public final class ConsoleDescriptionException extends Exception {
    public ConsoleDescriptionException() {
        super("YOU ENTERED INCORRECT DESCRIPTION");
    }
}
