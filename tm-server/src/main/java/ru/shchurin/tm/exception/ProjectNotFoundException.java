package ru.shchurin.tm.exception;

public final class ProjectNotFoundException extends Exception {
    public ProjectNotFoundException() {
        super("Project not found");
    }
}
