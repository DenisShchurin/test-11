package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.*;
import ru.shchurin.tm.endpoint.ProjectEndpointImpl;
import ru.shchurin.tm.endpoint.TaskEndpointImpl;
import ru.shchurin.tm.endpoint.UserEndpointImpl;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.repository.ProjectRepositoryImpl;
import ru.shchurin.tm.repository.TaskRepositoryImpl;
import ru.shchurin.tm.repository.UserRepositoryImpl;
import ru.shchurin.tm.service.ProjectServiceImpl;
import ru.shchurin.tm.service.TaskServiceImpl;
import ru.shchurin.tm.service.UserServiceImpl;

import javax.xml.ws.Endpoint;
import java.util.*;

public class BootstrapServer implements ServiceLocator {
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskRepository);

    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl();

    public void registryEndpoint() {
        Endpoint.publish("http://localhost:8080/ProjectEndpointImpl?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskEndpointImpl?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpointImpl?wsdl", userEndpoint);
    }

    public void initUsers() throws Exception {
        @NotNull final String adminHash = userService.getHashOfPassword("Admin");
        @NotNull final String userHash = userService.getHashOfPassword("User");
        userService.persist(new User("Admin", adminHash, Role.ROLE_ADMIN));

        @NotNull final User user = new User("User", userHash, Role.ROLE_USER);
        Project project = new Project("Project1", "project", new Date(), new Date(), user.getId());
        Task task = new Task("Task1", "description", project.getId(), new Date(), new Date(), user.getId());
        projectService.persist(project);
        taskService.persist(task);
        userService.persist(user);
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }


}
