package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserEndpoint {

    @WebMethod
    @NotNull
    List<User> findAllUsers();

    @WebMethod
    @Nullable
    User findOneUser(@WebParam(name = "id") @Nullable String id) throws Exception;

    @WebMethod
    void persistUser(@WebParam(name = "user") @Nullable final User user) throws Exception;

    @WebMethod
    void mergeUser(@WebParam(name = "user") @Nullable final User user) throws Exception;

    @WebMethod
    void removeUser(@WebParam(name = "id") @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsers();

    @WebMethod
    void removeUserByLogin(@WebParam(name = "login") @Nullable String login) throws Exception;

    @WebMethod
    @NotNull
    String getHashOfPassword(@WebParam(name = "password") @Nullable String password) throws Exception;

    @WebMethod
    @Nullable
    User authoriseUser(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "hashOfPassword") @Nullable String hashOfPassword) throws Exception;

    @WebMethod
    boolean updatePassword(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "hashOfPassword") @Nullable String hashPassword,
            @WebParam(name = "newHashOfPassword") @Nullable String newHashPassword) throws Exception;
}
