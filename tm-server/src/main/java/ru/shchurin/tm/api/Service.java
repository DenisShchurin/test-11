package ru.shchurin.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.AbstractEntity;

public interface Service<T extends AbstractEntity> {
    void persist(@Nullable T entity) throws Exception;

    void merge(@Nullable T entity) throws Exception;
}
