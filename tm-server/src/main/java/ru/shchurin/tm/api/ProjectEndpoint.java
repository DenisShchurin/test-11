package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProjectEndpoint {

    @NotNull
    @WebMethod
    List<Project> findAllProjects();

    @WebMethod
    @NotNull
    List<Project> findAllProjectsByUserId(@WebParam(name = "userId") @NotNull final String userId) throws Exception;

    @WebMethod
    @NotNull
    Project findOneProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void persistProject(@WebParam(name = "project") @Nullable final Project project) throws Exception;

    @WebMethod
    void mergeProject(@WebParam(name = "project") @Nullable final Project project) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllProjects(@WebParam(name = "userId") @Nullable final String userId) throws Exception;

    @WebMethod
    void removeByNameProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @NotNull
    List<Task> getTasksOfProjectByName(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    void removeProjectAndTasksByName(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "name") @Nullable final String name) throws Exception;

    @WebMethod
    @NotNull
    List<Project> findByNameProject(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception;

    @WebMethod
    @NotNull
    List<Project> findByDescriptionProject(
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception;

}
