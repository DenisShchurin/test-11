package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TaskEndpoint {

    @WebMethod
    @NotNull
    List<Task> findAllTasks();

    @WebMethod
    @NotNull
    List<Task> findAllTasksByUserId(@WebParam(name = "userId") @Nullable String userId) throws Exception;

    @WebMethod
    @NotNull
    Task findOneTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "id") @Nullable String id) throws Exception;

    @WebMethod
    void persistTask(@WebParam(name = "task") @Nullable final Task task) throws Exception;

    @WebMethod
    void mergeTask(@WebParam(name = "task") @Nullable final Task task) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "id") @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasks(@WebParam(name = "userId") @Nullable String userId) throws Exception;

    @WebMethod
    void removeByNameTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "name") @Nullable String name) throws Exception;

    @WebMethod
    @NotNull
    List<Task> findByNameTask(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception;

    @WebMethod
    @NotNull
    List<Task> findByDescriptionTask(
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId)
            throws Exception;
}
