package ru.shchurin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Task;

import java.util.Comparator;

public final class TaskEndDateComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull Task o1, @NotNull Task o2) {
        return o1.getEndDate().compareTo(o2.getEndDate());
    }
}
