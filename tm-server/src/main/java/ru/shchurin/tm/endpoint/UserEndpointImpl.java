package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.api.UserEndpoint;
import ru.shchurin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.api.UserEndpoint")
public final class UserEndpointImpl implements UserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    public UserEndpointImpl(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public UserEndpointImpl() {
    }

    @WebMethod
    @NotNull
    public List<User> findAllUsers() {
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    @Nullable
    public User findOneUser(@WebParam(name = "id") @Nullable String id) throws Exception {
        return serviceLocator.getUserService().findOne(id);
    }

    @WebMethod
    public void persistUser(@WebParam(name = "user") @Nullable final User user) throws Exception {
        serviceLocator.getUserService().persist(user);
    }

    @WebMethod
    public void mergeUser(@WebParam(name = "user") @Nullable final User user) throws Exception {
        serviceLocator.getUserService().merge(user);
    }

    @WebMethod
    public void removeUser(@WebParam(name = "id") @Nullable String id) throws Exception{
        serviceLocator.getUserService().remove(id);
    }

    @WebMethod
    public void removeAllUsers() {
        serviceLocator.getUserService().removeAll();
    }

    @WebMethod
    public void removeUserByLogin(@WebParam(name = "login") @Nullable String login) throws Exception {
        serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    @NotNull
    public String getHashOfPassword(@WebParam(name = "password") @Nullable String password) throws Exception {
        return serviceLocator.getUserService().getHashOfPassword(password);
    }

    @WebMethod
    @Nullable
    public User authoriseUser(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "hashOfPassword") @Nullable String hashOfPassword) throws Exception {
        return serviceLocator.getUserService().authoriseUser(login, hashOfPassword);
    }

    @WebMethod
    public boolean updatePassword(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "hashOfPassword") @Nullable String hashPassword,
            @WebParam(name = "newHashOfPassword") @Nullable String newHashPassword) throws Exception {
         return serviceLocator.getUserService().updatePassword(login, hashPassword, newHashPassword);
    }
}
