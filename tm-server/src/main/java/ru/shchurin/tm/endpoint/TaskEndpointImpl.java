package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.api.TaskEndpoint;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.api.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;


    public TaskEndpointImpl(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public TaskEndpointImpl() {
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findAllTasks() {
        return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findAllTasksByUserId(@WebParam(name = "userId") @Nullable String userId) throws Exception {
        return serviceLocator.getTaskService().findAll(userId);
    }

    @WebMethod
    @NotNull
    @Override
    public Task findOneTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "id") @Nullable String id) throws Exception {
        return serviceLocator.getTaskService().findOne(userId, id);
    }

    @WebMethod
    @Override
    public void persistTask(@WebParam(name = "task") @Nullable final Task task) throws Exception {
        serviceLocator.getTaskService().persist(task);
    }

    @WebMethod
    @Override
    public void mergeTask(@WebParam(name = "project") @Nullable final Task task) throws Exception {
        serviceLocator.getTaskService().merge(task);
    }

    @WebMethod
    @Override
    public void removeTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "id") @Nullable String id) throws Exception {
        serviceLocator.getTaskService().remove(userId, id);
    }

    @WebMethod
    @Override
    public void removeAllTasks(@WebParam(name = "userId") @Nullable String userId) throws Exception {
        serviceLocator.getTaskService().removeAll(userId);
    }

    @WebMethod
    @Override
    public void removeByNameTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "name") @Nullable String name) throws Exception {
        serviceLocator.getTaskService().removeByName(userId, name);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findByNameTask(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception {
        return serviceLocator.getTaskService().findByName(name, currentUserId);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findByDescriptionTask(
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId)
            throws Exception {
        return serviceLocator.getTaskService().findByDescription(description, currentUserId);
    }
}
