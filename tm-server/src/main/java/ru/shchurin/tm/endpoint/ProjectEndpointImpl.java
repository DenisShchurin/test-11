package ru.shchurin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ProjectEndpoint;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.shchurin.tm.api.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {
    @Nullable
    private ServiceLocator serviceLocator;

    public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public ProjectEndpointImpl() {
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findAllProjects() {
        return serviceLocator.getProjectService().findAll();
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findAllProjectsByUserId(@WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        return serviceLocator.getProjectService().findAll(userId);
    }

    @WebMethod
    @NotNull
    @Override
    public Project findOneProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id) throws Exception {
        return serviceLocator.getProjectService().findOne(userId, id);
    }

    @WebMethod
    @Override
    public void persistProject(@WebParam(name = "project") @Nullable final Project project) throws Exception {
        serviceLocator.getProjectService().persist(project);
    }

    @WebMethod
    @Override
    public void mergeProject(@WebParam(name = "project") @Nullable final Project project) throws Exception {
        serviceLocator.getProjectService().merge(project);
    }

    @WebMethod
    @Override
    public void removeProject(
            @WebParam(name = "userId") @Nullable final String userId, @WebParam(name = "id") @Nullable final String id)
            throws Exception {
        serviceLocator.getProjectService().remove(userId, id);
    }

    @WebMethod
    @Override
    public void removeAllProjects(@WebParam(name = "userId") @Nullable final String userId) throws Exception {
        serviceLocator.getProjectService().removeAll(userId);
    }

    @WebMethod
    public void removeByNameProject(
            @WebParam(name = "userId") @Nullable final String userId, @WebParam(name = "name") @Nullable final String name)
            throws Exception {
        serviceLocator.getProjectService().removeByName(userId, name);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> getTasksOfProjectByName(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "name") @Nullable final String name) throws Exception {
        return serviceLocator.getProjectService().getTasksOfProject(userId, name);
    }

    @WebMethod
    @Override
    public void removeProjectAndTasksByName(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "name") @Nullable final String name) throws Exception {
        serviceLocator.getProjectService().removeProjectAndTasksByName(userId, name);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findByNameProject(
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception {
        return serviceLocator.getProjectService().findByName(name, currentUserId);
    }

    @WebMethod
    @NotNull
    @Override
    public List<Project> findByDescriptionProject(
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "currentUserId") @NotNull final String currentUserId) throws Exception {
        return serviceLocator.getProjectService().findByDescription(description, currentUserId);
    }
}
